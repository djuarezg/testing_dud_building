# Plugin

`dud` is the name for both the Hub and the Builder plugin, so you can add it to their configuration.

** PLEASE NOTE THIS CODE IS NOW BEING REVIEWED ON https://pagure.io/koji/pull-request/3217, REFER TO IT INSTEAD **

# Testing Dud Building

[LOS-826](https://its.cern.ch/jira/browse/LOS-826)

Testing Disk Update Driver building prior to the Koji plugin implementation

```
wget https://elrepo.org/linux/elrepo/el7/SRPMS/wireguard-kmod-1.0.20201221-1.el7_9.elrepo.src.rpm
wget https://elrepo.org/linux/elrepo/el7/x86_64/RPMS/kmod-wireguard-1.0.20201221-1.el7_9.elrepo.x86_64.rpm
sudo ./mkdd.sh wireguard-kmod-1.0.20201221-1.el7_9.elrepo.src.rpm
```

The problem is that we need to create the package first.

# Koji plugin

* Using los826 env for lsb hg and koji module
* `vi /root/.koji/plugins/dud.py for the client`

```
koji -p kojitest add-tag --arches "x86_64 aarch64"  dud-build
koji -p kojitest add-group dud-build build
koji -p kojitest add-group dud-build srpm-build
koji -p kojitest add-group-pkg dud-build build bash bzip2 coreutils cpio diffutils redhat-release-everything findutils gawk gcc gcc-c++ grep gzip info make patch redhat-rpm-config rpm-build sed shadow-utils tar unzip util-linux-ng which tar cern-koji-addons buildsys-macros-el8
koji -p kojitest add-group-pkg dud-build srpm-build bash curl cvs redhat-release-everything gnupg make redhat-rpm-config rpm-build shadow-utils tar buildsys-macros-el8 cern-koji-addons

koji -p kojitest add-external-repo --tag=dud-build cs8-cern -p 60 --mode=bare
koji -p kojitest add-external-repo --tag=dud-build cs8-baseos -p 70 --mode=bare
koji -p kojitest add-external-repo --tag=dud-build cs8-appstream -p 80 --mode=bare
koji -p kojitest add-external-repo --tag=dud-build cs8-powertools -p 90 --mode=bare

koji -p kojitest add-tag dud

koji -p kojitest add-target dud dud-build dud
```



```
koji -p kojitest add-pkg dud-testing dud --owner djuarezg
```

```
# Add special group dud
[djuarezg@aiadm61 ~]$ koji -p kojitest add-group dud-build dud
Group dud already exists for tag dud-build
```

```
# mkisofs pkg comes from genisoimage
koji -p kojitest add-group-pkg dud-build dud genisoimage
koji -p kojitest add-group-pkg dud-build dud createrepo_c
koji -p kojitest add-group-pkg dud-build dud curl
koji -p kojitest add-group-pkg dud-build dud dnf
koji -p kojitest add-group-pkg dud-build dud dnf-plugins-core
koji -p kojitest edit-tag dud-build -x mock.new_chroot=False
```


```
[djuarezg@aiadm61 ~]$ koji -p kojitest add-group-pkg dud-build dud coreutils
[djuarezg@aiadm61 ~]$ koji -p kojitest add-group-pkg dud-build dud cpio
[djuarezg@aiadm61 ~]$ koji -p kojitest add-group-pkg dud-build dud cern-koji-addons
[djuarezg@aiadm61 ~]$ koji -p kojitest add-group-pkg dud-build dud bash
[djuarezg@aiadm61 ~]$ koji -p kojitest add-group-pkg dud-build dud bashbzip2
[djuarezg@aiadm61 ~]$ koji -p kojitest add-group-pkg dud-build dud diffutils
[djuarezg@aiadm61 ~]$ koji -p kojitest add-group-pkg dud-build dud findutils
[djuarezg@aiadm61 ~]$ koji -p kojitest add-group-pkg dud-build dud which
[djuarezg@aiadm61 ~]$ koji -p kojitest add-group-pkg dud-build dud shadow-utils
```
